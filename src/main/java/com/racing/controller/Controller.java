package com.racing.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import com.github.fge.jsonschema.report.ProcessingReport;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.racing.model.*;
import com.racing.model.player.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;

public class Controller {

    private Logger logger = LogManager.getLogger();
    private Player user;
    private ArrayList<Player> enemyList;
    private static Map<String, Map<Characteristic, Double>> characteristicPossibilities = new HashMap<String, Map<Characteristic, Double>>();

    public void setNewUser(String username, CarPossibilities car) {
        user = new Player(username, car);
        generateEnemies();
        setCharacteristicPossibilities();
    }

    public void setUser(String username) throws IOException, ProcessingException {
        File userJSON = new File("src/main/resources/" + username + ".json");
        File schema = new File("src/main/resources/JSONSchema.json");
        if(validate(userJSON,schema)) {
            Gson gson = new GsonBuilder().create();
            user = gson.fromJson(new FileReader(userJSON), Player.class);
        }
        generateEnemies();
        setCharacteristicPossibilities();
    }

    public void writeToJson() {
        try (OutputStreamWriter writer = new FileWriter(new File("src/main/resources/" + user.getName() + ".json"))) {
            new GsonBuilder().create().toJson(user, writer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean validate(final File json, final File jsonSchema)
            throws IOException, ProcessingException {
        JsonNode data = JsonLoader.fromFile(json);
        JsonNode schema = JsonLoader.fromFile(jsonSchema);

        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonValidator validator = factory.getValidator();
        ProcessingReport report = validator.validate(schema, data);

        return report.isSuccess();
    }

    private void generateEnemies() {
        enemyList = new ArrayList<>();
        String[] enemyNames = {"Mac", "Jessy", "Raw", "Lina", "Markus", "Ronnie"};
        for (String name : enemyNames) {
            Player enemy = new Player(name, CarPossibilities.values()[new Random().nextInt(CarPossibilities.values().length)], true);
            enemyList.add(enemy);
        }
    }

    public void race(int enemy) {
        if (getPosibility(user) == getPosibility(enemyList.get(enemy))) {
            logger.info("Draw");
            logger.info("You get 500");
            user.setWallet(500);
        } else if (getPosibility(user) > getPosibility(enemyList.get(enemy))) {
            logger.info("You win");
            logger.info("You get 1000");
            enemyList.remove(enemy);
            user.setWallet(1000);
        } else {
            logger.info("You lose");
        }
        if(enemyList.size()<=3){
            generateEnemies();
        }
    }

    private void setCharacteristicPossibilities() {
        characteristicPossibilities.put("Engine", new Possibilities(1.0, 2.0, 3.0).getMap());
        characteristicPossibilities.put("Transmission", new Possibilities(0.8, 1.5, 2.0).getMap());
        characteristicPossibilities.put("Brakes", new Possibilities(0.3, 0.9, 1.8).getMap());
        characteristicPossibilities.put("Suspension", new Possibilities(0.5, 1.0, 1.7).getMap());
        characteristicPossibilities.put("ExhaustDeveloped", new Possibilities(0.3, 0.8, 1.5).getMap());
    }

    private double getPosibility(Player player) {
        double possibility;
        possibility = player.getCar().getName().getPosibility()
                + characteristicPossibilities.get("Engine").get(player.getCar().getCharacteristic().getEngine())
                + characteristicPossibilities.get("Transmission").get(player.getCar().getCharacteristic().getTransmission())
                + characteristicPossibilities.get("Brakes").get(player.getCar().getCharacteristic().getBrakes())
                + characteristicPossibilities.get("Suspension").get(player.getCar().getCharacteristic().getSuspension())
                + characteristicPossibilities.get("ExhaustDeveloped").get(player.getCar().getCharacteristic().getExhaust());
        return possibility;
    }

    public ArrayList<Player> getEnemy() {
        return enemyList;
    }

    public void developEngine() {
        if (user.getCar().getCharacteristic().getEngine() == Characteristic.LOW && user.getWallet() >= user.getCar().getName().getEngineDeveloped().getMedium()) {
            user.getCar().getCharacteristic().setEngine(Characteristic.MEDIUM);
            user.setWallet(user.getWallet() - user.getCar().getName().getEngineDeveloped().getMedium());
        } else if (user.getCar().getCharacteristic().getEngine() == Characteristic.MEDIUM && user.getWallet() >= user.getCar().getName().getEngineDeveloped().getHigh()) {
            user.getCar().getCharacteristic().setEngine(Characteristic.HIGH);
            user.setWallet(user.getWallet() - user.getCar().getName().getEngineDeveloped().getHigh());
        } else {
            logger.info("You don`t have enough money!!!");
        }
    }

    public void developTransmissson() {
        if (user.getCar().getCharacteristic().getTransmission() == Characteristic.LOW && user.getWallet() >= user.getCar().getName().getTransmissionDeveloped().getMedium()) {
            user.getCar().getCharacteristic().setTransmission(Characteristic.MEDIUM);
            user.setWallet(user.getWallet() - user.getCar().getName().getTransmissionDeveloped().getMedium());
        } else if (user.getCar().getCharacteristic().getTransmission() == Characteristic.MEDIUM && user.getWallet() >= user.getCar().getName().getTransmissionDeveloped().getHigh()) {
            user.getCar().getCharacteristic().setTransmission(Characteristic.HIGH);
            user.setWallet(user.getWallet() - user.getCar().getName().getTransmissionDeveloped().getHigh());
        } else {
            logger.info("You don`t have enough money!!!");
        }
    }

    public void developSuspension() {
        if (user.getCar().getCharacteristic().getSuspension() == Characteristic.LOW && user.getWallet() >= user.getCar().getName().getSuspensionDeveloped().getMedium()) {
            user.getCar().getCharacteristic().setSuspension(Characteristic.MEDIUM);
            user.setWallet(user.getWallet() - user.getCar().getName().getSuspensionDeveloped().getMedium());
        } else if (user.getCar().getCharacteristic().getSuspension() == Characteristic.MEDIUM && user.getWallet() >= user.getCar().getName().getSuspensionDeveloped().getHigh()) {
            user.getCar().getCharacteristic().setSuspension(Characteristic.HIGH);
            user.setWallet(user.getWallet() - user.getCar().getName().getSuspensionDeveloped().getHigh());
        } else {
            logger.info("You don`t have enough money!!!");
        }
    }

    public void developBrakes() {
        if (user.getCar().getCharacteristic().getBrakes() == Characteristic.LOW && user.getWallet() >= user.getCar().getName().getBrakesDeveloped().getMedium()) {
            user.getCar().getCharacteristic().setBrakes(Characteristic.MEDIUM);
            user.setWallet(user.getWallet() - user.getCar().getName().getBrakesDeveloped().getMedium());
        } else if (user.getCar().getCharacteristic().getBrakes() == Characteristic.MEDIUM && user.getWallet() >= user.getCar().getName().getBrakesDeveloped().getHigh()) {
            user.getCar().getCharacteristic().setBrakes(Characteristic.HIGH);
            user.setWallet(user.getWallet() - user.getCar().getName().getBrakesDeveloped().getHigh());
        } else {
            logger.info("You don`t have enough money!!!");
        }
    }

    public void developExhaust() {
        if (user.getCar().getCharacteristic().getExhaust() == Characteristic.LOW && user.getWallet() >= user.getCar().getName().getExhaustDeveloped().getMedium()) {
            user.getCar().getCharacteristic().setExhaust(Characteristic.MEDIUM);
            user.setWallet(user.getWallet() - user.getCar().getName().getExhaustDeveloped().getMedium());
        } else if (user.getCar().getCharacteristic().getExhaust() == Characteristic.MEDIUM && user.getWallet() >= user.getCar().getName().getExhaustDeveloped().getHigh()) {
            user.getCar().getCharacteristic().setExhaust(Characteristic.HIGH);
            user.setWallet(user.getWallet() - user.getCar().getName().getExhaustDeveloped().getHigh());
        } else {
            logger.info("You don`t have enough money!!!");
        }
    }

    public int[] getPriceEngine() {
        int[] prices = new int[2];
        prices[0] = user.getCar().getName().getEngineDeveloped().getMedium();
        prices[1] = user.getCar().getName().getEngineDeveloped().getHigh();
        return prices;
    }

    public int[] getPriceTransmission() {
        int[] prices = new int[2];
        prices[0] = user.getCar().getName().getTransmissionDeveloped().getMedium();
        prices[1] = user.getCar().getName().getTransmissionDeveloped().getHigh();
        return prices;
    }

    public int[] getPriceSuspension() {
        int[] prices = new int[2];
        prices[0] = user.getCar().getName().getSuspensionDeveloped().getMedium();
        prices[1] = user.getCar().getName().getSuspensionDeveloped().getHigh();
        return prices;
    }

    public int[] getPriceBrakes() {
        int[] prices = new int[2];
        prices[0] = user.getCar().getName().getBrakesDeveloped().getMedium();
        prices[1] = user.getCar().getName().getBrakesDeveloped().getHigh();
        return prices;
    }

    public int[] getPriceExhaust() {
        int[] prices = new int[2];
        prices[0] = user.getCar().getName().getExhaustDeveloped().getMedium();
        prices[1] = user.getCar().getName().getExhaustDeveloped().getHigh();
        return prices;
    }

    public String info() {
        return user.toString();
    }

}
