package com.racing;

import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.racing.controller.Controller;
import com.racing.model.CarPossibilities;
import com.racing.model.player.Player;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.util.Scanner;

public class Application{

    private static Logger logger = LogManager.getLogger();
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        Controller controller = new Controller();

        while(true) {
            System.out.println("1.New game");
            System.out.println("2.Resume game");
            System.out.println("Enter");
            int index =sc.nextInt();
            if (index == 1) {
                System.out.println("Enter name");
                sc.nextLine();
                String username = sc.nextLine();
                CarPossibilities car;
                while(true) {
                    System.out.println("1.BMW");
                    System.out.println("2.Mercedes");
                    System.out.println("3.Audi");
                    System.out.println("Enter:");
                    int index1 = sc.nextInt();
                    if (index1 == 1) {
                        car = CarPossibilities.BMW;
                        break;
                    } else if (index1 == 2) {
                        car = CarPossibilities.MERCEDES;
                        break;
                    } else if (index1 == 3) {
                        car = CarPossibilities.AUDI;
                        break;
                    } else {
                        System.out.println("Wrong index");
                    }
                }
                controller.setNewUser(username,car);
                break;
            } else if (index == 2) {
                System.out.println("Enter your username");
                sc.nextLine();
                String username = sc.nextLine();
                try {
                    controller.setUser(username);
                } catch (ProcessingException | IOException e) {
                    e.printStackTrace();
                }
                break;
            }else{
                System.out.println("Wrong index");
            }
        }
        System.out.println(controller.info());
        controller.developEngine();
        controller.developTransmissson();
        logger.info(controller.info());

        for(Player player : controller.getEnemy()){
            System.out.println(player.toString());
        }
        System.out.println("Choose enemy");
        int choose = sc.nextInt();
        controller.race(choose-1);
//        final Thread thread = new Thread(new Runnable() {
//            public void run() {
//                while(true) {
//                    user.setWallet(50);
//                    try {
//                        Thread.sleep(2000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });
//        thread.start();

        controller.writeToJson();
        return;
    }













}
