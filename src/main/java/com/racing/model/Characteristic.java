package com.racing.model;

public enum Characteristic {
    LOW,
    MEDIUM,
    HIGH
}
