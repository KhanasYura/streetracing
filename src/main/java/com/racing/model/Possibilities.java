package com.racing.model;

import java.util.HashMap;
import java.util.Map;


public class Possibilities {
    Map<Characteristic,Double> map;
    public Possibilities(double value1, double value2, double value3){
        map = new HashMap<>();
        map.put(Characteristic.LOW,value1);
        map.put(Characteristic.MEDIUM, value2);
        map.put(Characteristic.HIGH,value3);
    }

    public Map<Characteristic, Double> getMap() {
        return map;
    }

    public void setMap(Map<Characteristic, Double> map) {
        this.map = map;
    }
}
