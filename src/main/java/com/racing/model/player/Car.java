package com.racing.model.player;

import com.racing.model.CarPossibilities;
import com.racing.model.Characteristic;

import java.util.Random;

public class Car {
    private CarPossibilities name;
    private CarCharacteristic characteristic;

    public Car(CarPossibilities car_name) {
        name = car_name;
        characteristic = new CarCharacteristic();
    }

    public Car(CarPossibilities car_name, Characteristic[] characteristics) {
        name = car_name;
        characteristic = new CarCharacteristic(characteristics);
    }

    public void randomCharacteristics() {
        Characteristic[] levels = {Characteristic.LOW, Characteristic.MEDIUM, Characteristic.HIGH};
        int characteristicLength = 5;
        Characteristic[] characteristics = new Characteristic[characteristicLength];
        Random random = new Random();
        for (int i = 0; i < characteristicLength; i++) {
            characteristics[i] = levels[random.nextInt(levels.length)];
        }
        characteristic = new CarCharacteristic(characteristics);
    }

    public CarPossibilities getName() {
        return name;
    }

    public void setName(CarPossibilities name) {
        this.name = name;
    }

    public CarCharacteristic getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(CarCharacteristic characteristic) {
        this.characteristic = characteristic;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name=" + name +
                ", characteristic=" + characteristic +
                '}';
    }
}

