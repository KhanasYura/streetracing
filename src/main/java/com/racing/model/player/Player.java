package com.racing.model.player;

import com.racing.model.CarPossibilities;
import com.racing.model.Characteristic;

public class Player {
    private String name;
    private Car car;
    private long wallet = 50000;


    public Player(String user_name, CarPossibilities car_name){
        name = user_name;
        car = new Car(car_name);
    }
    public Player(String user_name, CarPossibilities car_name, Boolean characteristic){
        name = user_name;
        car = new Car(car_name);
        if(characteristic) {
            car.randomCharacteristics();
        }
    }

    public Player(String user_name, CarPossibilities car_name, Characteristic[] characteristics){
        name = user_name;
        car = new Car(car_name, characteristics);
    }



    @Override
    public String toString(){
        String info = "Username - " + name + ". Car - " + car.getName() + ".\nEngine - "
                + car.getCharacteristic().getEngine() + ".\nTransmission - "
                + car.getCharacteristic().getTransmission()+ ".\nBrakes - "
                + car.getCharacteristic().getBrakes() + ".\nSuspension - "
                + car.getCharacteristic().getSuspension() + ".\nExhaustDeveloped - "
                + car.getCharacteristic().getExhaust() +".\n\n";
        return info;
    }

    public long getWallet() {
        return wallet;
    }

    public void setWallet(long wallet) {
        this.wallet += wallet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
