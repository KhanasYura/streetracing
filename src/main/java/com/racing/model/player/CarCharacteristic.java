package com.racing.model.player;

import com.racing.model.Characteristic;

public class CarCharacteristic {
    private Characteristic engine = Characteristic.LOW;
    private Characteristic transmission = Characteristic.LOW;
    private Characteristic brakes = Characteristic.LOW;
    private Characteristic suspension = Characteristic.LOW;
    private Characteristic exhaust = Characteristic.LOW;

    public CarCharacteristic(){}

    public CarCharacteristic(Characteristic[] characteristics){
        engine = characteristics[0];
        transmission = characteristics[1];
        brakes = characteristics[2];
        suspension = characteristics[3];
        exhaust = characteristics[4];
    }

    public Characteristic getEngine() {
        return engine;
    }

    public void setEngine(Characteristic engine) {
        this.engine = engine;
    }

    public Characteristic getTransmission() {
        return transmission;
    }

    public void setTransmission(Characteristic transmission) {
        this.transmission = transmission;
    }

    public Characteristic getBrakes() {
        return brakes;
    }

    public void setBrakes(Characteristic brakes) {
        this.brakes = brakes;
    }

    public Characteristic getSuspension() {
        return suspension;
    }

    public void setSuspension(Characteristic suspension) {
        this.suspension = suspension;
    }

    public Characteristic getExhaust() {
        return exhaust;
    }

    public void setExhaust(Characteristic exhaust) {
        this.exhaust = exhaust;
    }

    @Override
    public String toString() {
        return "CarCharacteristic{" +
                "engine=" + engine +
                ", transmission=" + transmission +
                ", brakes=" + brakes +
                ", suspension=" + suspension +
                ", exhaust=" + exhaust +
                '}';
    }
}
