package com.racing.model;

import com.racing.model.cardevelopment.*;

public enum CarPossibilities {
    LAMBORGHINI(85, EngineDeveloped.LAMBORGHINI, TransmissionDeveloped.LAMBORGHINI, SuspensionDeveloped.LAMBORGHINI, BrakesDeveloped.LAMBORGHINI, ExhaustDeveloped.LAMBORGHINI),
    MCLAUREN(65,EngineDeveloped.MCLAUREN,TransmissionDeveloped.MCLAUREN,SuspensionDeveloped.MCLAUREN,BrakesDeveloped.MCLAUREN,ExhaustDeveloped.MCLAUREN),
    FERRARI(50,EngineDeveloped.FERRARI,TransmissionDeveloped.FERRARI,SuspensionDeveloped.FERRARI,BrakesDeveloped.FERRARI,ExhaustDeveloped.FERRARI),
    TESLA(35,EngineDeveloped.TESLA,TransmissionDeveloped.TESLA,SuspensionDeveloped.TESLA,BrakesDeveloped.TESLA,ExhaustDeveloped.TESLA),
    PORSCHE(32,EngineDeveloped.PORSCHE,TransmissionDeveloped.PORSCHE,SuspensionDeveloped.PORSCHE,BrakesDeveloped.PORSCHE,ExhaustDeveloped.PORSCHE),
    MUSTANG(27,EngineDeveloped.MUSTANG,TransmissionDeveloped.MUSTANG,SuspensionDeveloped.MUSTANG,BrakesDeveloped.MUSTANG,ExhaustDeveloped.MUSTANG),
    DODGE(23,EngineDeveloped.DODGE,TransmissionDeveloped.DODGE,SuspensionDeveloped.DODGE,BrakesDeveloped.DODGE,ExhaustDeveloped.DODGE),
    BMW(17,EngineDeveloped.BMW,TransmissionDeveloped.BMW,SuspensionDeveloped.BMW,BrakesDeveloped.BMW,ExhaustDeveloped.BMW),
    MERCEDES(16,EngineDeveloped.MERCEDES,TransmissionDeveloped.MERCEDES,SuspensionDeveloped.MERCEDES,BrakesDeveloped.MERCEDES,ExhaustDeveloped.MERCEDES),
    AUDI(15,EngineDeveloped.AUDI,TransmissionDeveloped.AUDI,SuspensionDeveloped.AUDI,BrakesDeveloped.AUDI,ExhaustDeveloped.AUDI);

    private final int possibility;
    private final EngineDeveloped engineDeveloped;
    private final TransmissionDeveloped transmissionDeveloped;
    private final SuspensionDeveloped suspensionDeveloped;
    private final BrakesDeveloped brakesDeveloped;
    private final ExhaustDeveloped exhaustDeveloped;

    CarPossibilities(int possibility, EngineDeveloped engineDeveloped, TransmissionDeveloped transmissionDeveloped,
                     SuspensionDeveloped suspensionDeveloped, BrakesDeveloped brakesDeveloped, ExhaustDeveloped exhaustDeveloped) {
        this.possibility = possibility;
        this.engineDeveloped = engineDeveloped;
        this.transmissionDeveloped = transmissionDeveloped;
        this.suspensionDeveloped = suspensionDeveloped;
        this.brakesDeveloped = brakesDeveloped;
        this.exhaustDeveloped = exhaustDeveloped;
    }

    public int getPosibility() {
        return possibility;
    }

    public EngineDeveloped getEngineDeveloped() {
        return engineDeveloped;
    }

    public TransmissionDeveloped getTransmissionDeveloped() {
        return transmissionDeveloped;
    }

    public SuspensionDeveloped getSuspensionDeveloped() {
        return suspensionDeveloped;
    }

    public BrakesDeveloped getBrakesDeveloped() {
        return brakesDeveloped;
    }

    public ExhaustDeveloped getExhaustDeveloped() {
        return exhaustDeveloped;
    }

}
