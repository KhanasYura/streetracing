package com.racing.model.cardevelopment;

public enum SuspensionDeveloped {
    LAMBORGHINI(4000,5000),
    MCLAUREN(3500,4500),
    FERRARI(3250,4400),
    TESLA(2500,3500),
    PORSCHE(2600,3500),
    MUSTANG(2000,3000),
    DODGE(2100,2900),
    BMW(2000,2400),
    MERCEDES(1900,2400),
    AUDI(1600,2200);

    private final int medium;
    private final int high;

    SuspensionDeveloped(int medium, int high) {
        this.medium = medium;
        this.high = high;
    }

    public int getMedium() {
        return medium;
    }

    public int getHigh() {
        return high;
    }
}
